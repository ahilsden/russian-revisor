@extends('layout')

@section('assets')
    <link href="{{ asset('css/nouns.css') }}" rel="stylesheet">
@endsection

@section('content')
    <h1>Nouns</h1>
    <h2>Tables showing M / F nouns which end in ь (soft) sign</h2>

    @if ($nouns->isNotEmpty())
        <table class="male">
            <tr>
                <th>English</th>
                <th>Russian</th>
                <th>Notes</th>
            </tr>
            @foreach($nouns as $noun)
                @if($noun->gender === 'm')
                <tr>
                    <td>{{ $noun->english }}</td>
                    <td>{{ $noun->russian }}</td>
                    <td>{{ $noun->notes }}</td>
                </tr>
                @endif
            @endforeach
        </table>
        <table class="female">
            <tr>
                <th>English</th>
                <th>Russian</th>
                <th>Notes</th>
            </tr>
            @foreach($nouns as $noun)
                @if($noun->gender === 'f')
                <tr>
                    <td>{{ $noun->english }}</td>
                    <td>{{ $noun->russian }}</td>
                    <td>{{ $noun->notes }}</td>
                </tr>
                @endif
            @endforeach
        </table>
    @else
        <p>Nothing to display</p>
    @endif

    <form method="post" action="/import">
        @csrf
        <button type="submit" name="submit" class="btn btn-primary" id="import">Import CSV from Storage</button>
    </form>

@endsection
