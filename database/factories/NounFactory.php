<?php

namespace Database\Factories;

use App\Models\Noun;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class NounFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \App\Models\Noun::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'english' => $this->faker->name(),
            'russian' => $this->faker->name(),
            'gender' => $this->faker->name(),
            'notes' => $this->faker->name(),
        ];
    }
}
