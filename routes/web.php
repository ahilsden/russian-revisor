<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Nouns;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Nouns::class, 'show']);
// todo: Remove this route when ajax in place
Route::post('/import', [Nouns::class, 'import']);
Route::post('/import-ajax', [Nouns::class, 'importAjax']);
