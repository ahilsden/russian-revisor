$(document).ready(() => {
    $('#import').click(event => {
        event.preventDefault();

        $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
        });

        $.ajax({
            type: 'POST',
            url: '/import',
            dataType: 'json',
            success: function(data) {
                if (data.result !== 'success') {
                    // todo: failure modal
                    $('.alert').html('Error');
                } else {
                    // todo: success modal
                    location.href = '/';
                }
            },
            error: function(data) {
                // todo: error handling
            }
        });
    });
});
