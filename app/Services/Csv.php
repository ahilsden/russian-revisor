<?php

namespace App\Services;

use App\Contracts\Services\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Csv implements File
{
    private string $csvName;
    private array $nounsInRussian = [];

    public function __construct(string $csvName)
    {
        $this->csvName = $csvName;
    }

    public function read(): array
    {
        $fileName = Storage::path('csv/' . $this->csvName . '.csv');
        $resource = fopen($fileName, "r");

        $csvData = [];

        if ($resource) {
            // todo: test more than 1000 entries
            while (($row = fgetcsv($resource, 100, ",")) !== false) {
                if ($this->hasInsufficientFields($row)) {
                    continue;
                }

                if ($this->isDuplicatedInCsv($row)) {
                    continue;
                }

                if ($this->isDuplicatedInDb($row)) {
                    continue;
                }

                $this->nounsInRussian[] = $row[1];
                $csvData[] = $row;
            }

            fclose($resource);
        }

        return $this->formatData($csvData);
    }

    private function hasInsufficientFields($row): bool
    {
        if (empty($row[0]) || empty($row[1]) || empty($row[2])) {
            return true;
        }

        return false;
    }

    private function isDuplicatedInCsv($row): bool
    {
        return in_array($row[1], $this->nounsInRussian);
    }

    private function isDuplicatedInDb($row): bool
    {
        return (DB::table('nouns')
            ->where('russian', $row[1])
            ->exists());
    }

    // todo: probably unnecessary to format here
    private function formatData(array $csvData): array
    {
        $formattedData = [];

        foreach ($csvData as $rowKey => $row) {
            $formattedData[$rowKey]['english'] = $row[0];
            $formattedData[$rowKey]['russian'] = $row[1];
            $formattedData[$rowKey]['gender'] = $row[2];
            $formattedData[$rowKey]['notes'] = $row[3] ?? null;
        }

        return $formattedData;
    }
}
