<?php

namespace App\Providers;

use App\Contracts\Services\File;
use App\Services\Csv;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(File::class, function () {
            // todo: if local, pass in noun-sample. If prod., something else:
            return new Csv('noun-sample');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
