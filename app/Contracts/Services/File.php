<?php

namespace App\Contracts\Services;

interface File
{
    public function read();
}
