<?php

namespace App\Http\Controllers;

use App\Contracts\Services\File;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Database\QueryException;

class Nouns extends Controller
{
    private File $file;

    public function __construct(File $file)
    {
        $this->file = $file;
    }

    public function show(): View
    {
        $nouns = DB::table('nouns')
            ->select(['english', 'russian', 'gender', 'notes'])
            ->get();

        return view('nouns.show', ['nouns' => $nouns]);
    }

    public function import(): JsonResponse
    {
        $nouns = $this->file->read();
        // todo: think this is queryBuilder rather than ORM (Eloquent)

        try {
            foreach ($nouns as $noun) {
                DB::table('nouns')
                    ->insert([
                        'english' => $noun['english'],
                        'russian' => $noun['russian'],
                        'gender' => $noun['gender'],
                        'notes' => $noun['notes'],
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
            }

            return response()->json(['result' => 'success']);

        } catch(QueryException $exception) {

            return response()->json(['result' => 'error']);
        }
    }
}
