<?php

namespace Tests\Unit\Services;

use Illuminate\Support\Facades\Storage;
use org\bovigo\vfs\vfsStream;
use Tests\TestCase;

class CsvTest extends TestCase
{
    /** @test */
    // todo: remove if not going to use vfsStream
    public function validFileSystemIsSetUpTest(): void
    {
        $csvStructure = [
            'data' => [
                'names.csv' => "Mr John Smith,
                                Mrs Jane Smith,
                                Mister John Doe"
            ],
        ];

        $root = vfsStream::setup('root', null, $csvStructure);

        $this->assertTrue($root->hasChild('data/names.csv'));
    }

    /** @test */
    public function shouldReadAValidCsvFileFromStorageTest(): void
    {
        $fileName = Storage::path('csv/testing/service-test.csv');
        $resource = fopen($fileName, "r");

        $csvData = [];
        if ($resource) {
            while (($row = fgetcsv($resource, 1000, ",")) !== false) {
                $csvData[] = $row;
            }

            fclose($resource);
        }

        $expected = [
            ['ночь', 'night', 'f', 'some notes'],
            ['жизнь', 'life', 'f', 'some notes'],
        ];

        $this->assertEquals($csvData, $expected);
    }
}

