<?php

namespace Tests\Unit\Controllers;

use App\Contracts\Services\File;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NounsTest extends TestCase
{
    use RefreshDatabase;

    private File $file;

    /** @test */
    public function showTest(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test */
    public function importTest(): void
    {
        $response = $this->post('import');

        $response->assertStatus(200);
    }
}
