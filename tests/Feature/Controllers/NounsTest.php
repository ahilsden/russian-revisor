<?php

namespace Tests\Feature\Controllers;

use App\Contracts\Services\File;
use App\Http\Controllers\Nouns;
use App\Models\Noun;
use App\Services\Csv;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NounsTest extends TestCase
{
    use RefreshDatabase;

    private File $file;
    private string $testName;

    public function setUp(): void
    {
        parent::setUp();

        $this->app->bind(File::class, function() {
            return new Csv( 'testing/' . $this->testName);
        });
    }

    /** @test */
    public function userCanImportNounsFromStorageTest(): void
    {
        // todo: authentication

        $this->testName = 'controller-test';

        $numberOfValidCsvLines = 2;

        $this->runImportProcess();

        $this->assertDatabaseCount('nouns', $numberOfValidCsvLines);
    }

    /** @test */
    public function userCannotImportANounThatIsDuplicatedInDbTest(): void
    {
        $this->testName = 'controller-test-duplicate-db';

        Noun::factory()->create([
            'english' => 'night',
            'russian' => 'ночь',
            'gender' => 'f',
            'notes' => 'some notes'
        ]);

        $numberOfValidCsvLines = 2;

        $this->runImportProcess();

        $this->assertDatabaseCount('nouns', $numberOfValidCsvLines);
    }

    /** @test */
    public function userCannotImportANounThatIsDuplicatedInCsvTest(): void
    {
        $this->testName = 'controller-test-duplicate-file';

        $numberOfValidCsvLines = 2;

        $this->runImportProcess();

        $this->assertDatabaseCount('nouns', $numberOfValidCsvLines);
    }

    /** @test */
    public function userCannotImportARowThatHasInsufficientFieldsTest(): void
    {
        $this->testName = 'controller-test-insufficient-fields';

        $numberOfValidCsvLines = 0;

        $this->runImportProcess();

        $this->assertDatabaseCount('nouns', $numberOfValidCsvLines);
    }

    private function runImportProcess(): void
    {
        $nouns = app(Nouns::class);
        $nouns->import();
    }
}
